======================================================================
PROJET FLASK 2A de Moqim GHIZLAN, Redwan OMARI, Bilal ESSALAK ELOURRAK
======================================================================

-----------
DESCRIPTION
-----------

Ce projet consiste à un site web developpé en Flask permettant d'acceder à une liste de cartes graphiques disponibles. Vous pouvez les 
acheter ou les ajouter dans votre favorie. Et vous avez la possibilité de modifier ce dernier. Si vous avez le grade administrateur alors 
vus pourrez directement modifier un article ou le supprimer de la site. En conclusion ce site est parfait pour toute personne souhaitant monter 
son propre PC.




---------------------
ETAT DU PROJET
---------------------

Le projet est entierement terminé et possède toutes les fonctionnalités desirées:
    Pour un utilisateur notmel, il peut consolter les articles, les acheter ou bien les aujouter à la liste de favorie.
    Pour un admin, il peut ajouter des articles sur le site, les modifier ou les supprimer, dans le cas où il ajoute un nouvelle article, si l'entreprise n'existe pas, elle sera automatiquement ajouter et l'article sera à son rôle associé à cette entreprise !!
    Pour créer un compte d'un admin, vous devez priser ce dernier dans les paramètre de la fonction (new_user) dans la page views.py
    Pour vous facilité les tests, nous avons mis en place un compte d'admin avec l'id = 1 et avec les informations :
        email    : email_pour_tester@iut.fr
        password : ThisIsAPassword2022
----------------
LANCER LE PROJET
----------------

Pour vous aider à lancer notre site, voici la liste des étapes à effectuer.
Etape 1 :
    Vous devez avoir le système sqlite3 sur votre imparti .
Etape 2 :
    Installation et activation de l'environnement de travail et lancer le site
    RDV dans la dossier carte graphiques, ensuite ouvrez un terminal.
        Si vous êtes sur Windows, tapez 'pip install virtualenv' ensuite 'python3 -m venv env'
        Si vous êtes sur Lunix, tapez 'pip install virtualenv' ensuite 'py -m venv env'
    Une fois l'environnement est installé, vous dovez l'activer.
        Si vous êtes sur Windows, tapez 'source env/bin/activate'
        Si vous êtes sur Lunix, tapez 'source env/Script/activate'
    Une fois l'environnement est activé, vous dovez installer les pakeges
        Taper 'pip install -r requirements.txt'
    Une fois les pakeges installées et dans la même terminal.
        Vous dovez créer la base de données. Tapez 'python run.py create'
    Une fois ce dernier est terminé. et dans la même terminal.
        Si vous êtes sur Windows, tapez 'python run.py'
        Si vous êtes sur Lunix, tapez 'python3 run.py'
    une fois le site est lancé, RDV sur le site 'localhost:5000'