from flask_sqlalchemy import SQLAlchemy 

from app import app

#create database connection object 

db = SQLAlchemy(app)

class Utilisateur(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    full_name = db.Column(db.String(50))
    email = db.Column(db.String(100))
    password = db.Column(db.String(50))
    role = db.Column(db.String(50))


class Entreprise(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50))



class Article(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50))
    entreprise = db.Column(db.String(50))
    prix = db.Column(db.Float)
    url = db.Column(db.String(1000))
    img = db.Column(db.String(1000))
    entreprise_id = db.Column(db.Integer,db.ForeignKey("entreprise.id"))

db.create_all()

