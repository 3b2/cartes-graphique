from flask import Flask, render_template, flash, redirect, request, url_for
from flask_login import login_user, login_required, logout_user, current_user
#from views import *
import os
from werkzeug.utils import secure_filename
#from modele import *



app = Flask(__name__)
UPLOAD_FOLDER = './static/images'
app.config['SECRET_KEY'] = 'hjshjhdjah kjshkjdhjs'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///myapp.db'




def check_password_hash(email, password):
    return True









def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))



liste_article = [
        {
            "id" : 1,
            "img" : "ASUS ROG Strix Z690-F Gaming WiFi – Carte.jpg",
            "name" : "ASUS ROG Strix Z690-F Gaming WiFi – Carte",
            "company" : "MSI",
            "price" : "500,79€",
            "url" : "https://www.amazon.fr/dp/B09JSZZXD4/ref=redir_mobile_desktop?_encoding=UTF8&aaxitk=f6c77b5ed1fd5b4a097f069086f043d8&hsa_cr_id=8197731350802&pd_rd_plhdr=t&pd_rd_r=460d9b36-5020-44e0-818b-18ac01cb914b&pd_rd_w=aL4To&pd_rd_wg=M1xuu&ref_=sbx_be_s_sparkle_mcd_asin_0_img",
            },{
            "id" : 2,
            "img" : "LIKJ_Carte Graphique, 128 Bits 4 Go DDR5 1250 MHz.jpg",
            "name" : "Carte Graphique, 128 Bits 4 Go DDR5 1250 MHz",
            "company" : "LIKJ",
            "price" : "177,69€",
            "url" : "https://www.amazon.fr/Graphique-Reconnaissance-Automatique-Traitement-Portable/dp/B09G3GM5SP/ref=sr_1_2_sspa?keywords=carte+graphique&qid=1641088834&sprefix=carte+g%2Caps%2C72&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFCSTVETVFWOVBSRkkmZW5jcnlwdGVkSWQ9QTAyNTY4MjFSTDFWR08zNUQwS0omZW5jcnlwdGVkQWRJZD1BMDczMTY1OTJGTEVXN05OVVI4UDImd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl",
            },{
            "id" : 3,
            "img" : "Gigabyte GeForce GTX 1050 Ti Windforce OC Edition 4G Carte Graphique.jpg",
            "name" : "Gigabyte GeForce GTX 1050 Ti Windforce OC Edition 4G Carte Graphique",
            "company" : "Gigabyte",
            "price" : "458,27€",
            "url" : "https://www.amazon.fr/Gigabyte-GeForce-Windforce-Carte-Graphique/dp/B01MEHGRMS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=yw7oa&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=SGQK1SSKB4722E1MD28R&pd_rd_r=07aaae78-4cbe-4812-a6ce-6020e8332557&pd_rd_wg=3A2Cv&pd_rd_i=B01MEHGRMS&psc=1",
            },{
            "id" : 4,
            "img" : "Asus CERBERUS-GTX1050TI-A4G.jpg",
            "name" : "Asus CERBERUS-GTX1050TI-A4G",
            "company" : "Asus ",
            "price" : "378,95€",
            "url" : "https://www.amazon.fr/CERBERUS-GTX1050TI-A4G-Carte-Graphique-Nvidia-Express/dp/B078ZT22SS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=8A0ze&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=52VSPM7R82SHB2W2C2XN&pd_rd_r=4c7c434c-2ef2-4c54-be40-d280a53b0e68&pd_rd_wg=LHvkc&pd_rd_i=B078ZT22SS&psc=1",
            }
        ]



liste_article_favorie = [{
            "id" : 4,
            "img" : "Asus CERBERUS-GTX1050TI-A4G.jpg",
            "name" : "Asus CERBERUS-GTX1050TI-A4G",
            "company" : "Asus ",
            "price" : "378,95€",
            "url" : "https://www.amazon.fr/CERBERUS-GTX1050TI-A4G-Carte-Graphique-Nvidia-Express/dp/B078ZT22SS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=8A0ze&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=52VSPM7R82SHB2W2C2XN&pd_rd_r=4c7c434c-2ef2-4c54-be40-d280a53b0e68&pd_rd_wg=LHvkc&pd_rd_i=B078ZT22SS&psc=1",
            }
]

def get_role_by_email(email):
    return "admin"
    
def get_article_by_id(id):
    for i in get_data():
        if i["id"] == id:
            return i
def update_article(id, article_name, company, url, price, img):
    for i in liste_article:
        if i['id'] == id:
            i['name'] = article_name
            i['company'] = company
            i['price'] = price
            i['url'] = url
            i['img'] = img
def get_data():
    return liste_article

def get_article_favorie_by_id():
    return liste_article_favorie
def add_to_favorie(id):
    for i in liste_article:
        if i['id'] == id:
            liste_article_favorie.append(i)
def delete(id):
    for i in range(len(liste_article)):
        if liste_article[i]["id"] == id:
            liste_article.pop(i-1)


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        user = True#user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(email, password):
                #login_user(user, remember=True)
                print(email, password, "1")
                return redirect(url_for('home'))
            else:
                print(email, password, "2")
                flash('Mot de passe incorrect, réessayez.', category='error')
        else:
            print(email, password, "3")
            flash("L'e-mail n'existe pas.", category='error')

    return render_template("index.html", role = get_role_by_email("email"))


@app.route("/admin/article/delete/<int:id>", methods=['GET', 'POST'])
def delete_article(id):
    delete(id)
    redirect(url_for('home'))






@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        full_name = request.form.get('full_name')
        email = request.form.get('email')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        user = False#user = User.query.filter_by(email=email).first()
        if user:
            flash("l'email existe déjà .", category='error')
        elif password1 != password2:
            flash('Les mots de passe ne correspondent pas .', category='error')
        elif len(password1) < 7:
            flash('Le mot de passe doit comporter au moins 7 caractères.', category='error')
        else:
            #new_user(full_name, email, password1)
            #login_user(new_user, remember=True)
            flash('Compte créé !', category='success')
            return redirect(url_for('home'))
    return render_template("signup.html", title="Créer un compte")

@app.route('/home', methods=['GET', 'POST'])
def home():
    if request.method == "POST":
        home_search = request.form.get('home_search')
        print(home_search)
        redirect(url_for('home'))
    return render_template("home.html", role = get_role_by_email("email"), data = get_data())

@app.route('/admin/article/add', methods=['GET', 'POST'])
def add_article():
    if request.method == 'POST':
        article_name = request.form.get('article_name')
        article_url = request.form.get('article_url')
        article_price = request.form.get('article_price')
        article_img = request.form.get('article_img')
        company_name = request.form.get('company_name')
        flash("Article ajoutée !", category='success')
        return redirect(url_for('home'))
    return render_template("add_article.html")




@app.route('/logout')
def logout():
    #logout_user()
    return redirect(url_for('index'))






@app.route("/favored/list", methods=['GET', 'POST'])
def liste_favored():
    return render_template("liste_favored.html", data= get_article_favorie_by_id())



@app.route("/admin/article/edit/<int:id>", methods=['GET', 'POST'])
def edit_article(id):
    if request.method == 'POST':
        update_article(id, request.form.get('article_name'), request.form.get('company_name'), request.form.get('article_url'), request.form.get('article_price'), request.form.get('article_img'))
        flash(f'article changé > {id}', category='error')
        redirect(url_for('home'))
    return render_template("edit_article.html", data= get_article_by_id(id))







if __name__ == '__main__':
    app.run(debug=True, port=8000)
"""
def new_article(nom, entreprise, price, url, img):
    n_a = Article(
            nom= article_name,
            entreprise = company_name,
            price = article_price,
            url = article_url,
            img = article_img
            )
    db.session.add(n_a)
    db.session.commit()
def new_user(full_name, email, password1):
    n_u = Utilsateur(full_name = full_name, email = email, password = password1)
    db.session?add(n_u)
    db.session.commit()



"""
