liste_article = [
        {
            "id" : 1,
            "img" : "ASUS ROG Strix Z690-F Gaming WiFi – Carte.jpg",
            "name" : "ASUS ROG Strix Z690-F Gaming WiFi – Carte",
            "company" : "MSI",
            "price" : "500,79€",
            "url" : "https://www.amazon.fr/dp/B09JSZZXD4/ref=redir_mobile_desktop?_encoding=UTF8&aaxitk=f6c77b5ed1fd5b4a097f069086f043d8&hsa_cr_id=8197731350802&pd_rd_plhdr=t&pd_rd_r=460d9b36-5020-44e0-818b-18ac01cb914b&pd_rd_w=aL4To&pd_rd_wg=M1xuu&ref_=sbx_be_s_sparkle_mcd_asin_0_img",
            },{
            "id" : 2,
            "img" : "LIKJ_Carte Graphique, 128 Bits 4 Go DDR5 1250 MHz.jpg",
            "name" : "Carte Graphique, 128 Bits 4 Go DDR5 1250 MHz",
            "company" : "LIKJ",
            "price" : "177,69€",
            "url" : "https://www.amazon.fr/Graphique-Reconnaissance-Automatique-Traitement-Portable/dp/B09G3GM5SP/ref=sr_1_2_sspa?keywords=carte+graphique&qid=1641088834&sprefix=carte+g%2Caps%2C72&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFCSTVETVFWOVBSRkkmZW5jcnlwdGVkSWQ9QTAyNTY4MjFSTDFWR08zNUQwS0omZW5jcnlwdGVkQWRJZD1BMDczMTY1OTJGTEVXN05OVVI4UDImd2lkZ2V0TmFtZT1zcF9hdGYmYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl",
            },{
            "id" : 3,
            "img" : "Gigabyte GeForce GTX 1050 Ti Windforce OC Edition 4G Carte Graphique.jpg",
            "name" : "Gigabyte GeForce GTX 1050 Ti Windforce OC Edition 4G Carte Graphique",
            "company" : "Gigabyte",
            "price" : "458,27€",
            "url" : "https://www.amazon.fr/Gigabyte-GeForce-Windforce-Carte-Graphique/dp/B01MEHGRMS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=yw7oa&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=SGQK1SSKB4722E1MD28R&pd_rd_r=07aaae78-4cbe-4812-a6ce-6020e8332557&pd_rd_wg=3A2Cv&pd_rd_i=B01MEHGRMS&psc=1",
            },{
            "id" : 4,
            "img" : "Asus CERBERUS-GTX1050TI-A4G.jpg",
            "name" : "Asus CERBERUS-GTX1050TI-A4G",
            "company" : "Asus ",
            "price" : "378,95€",
            "url" : "https://www.amazon.fr/CERBERUS-GTX1050TI-A4G-Carte-Graphique-Nvidia-Express/dp/B078ZT22SS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=8A0ze&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=52VSPM7R82SHB2W2C2XN&pd_rd_r=4c7c434c-2ef2-4c54-be40-d280a53b0e68&pd_rd_wg=LHvkc&pd_rd_i=B078ZT22SS&psc=1",
            }
        ]



liste_article_favorie = [{
            "id" : 4,
            "img" : "Asus CERBERUS-GTX1050TI-A4G.jpg",
            "name" : "Asus CERBERUS-GTX1050TI-A4G",
            "company" : "Asus ",
            "price" : "378,95€",
            "url" : "https://www.amazon.fr/CERBERUS-GTX1050TI-A4G-Carte-Graphique-Nvidia-Express/dp/B078ZT22SS/ref=pd_sbs_1/259-3210224-8490231?pd_rd_w=8A0ze&pf_rd_p=81cea6b9-d758-4c09-83c8-cd14bfe8b85f&pf_rd_r=52VSPM7R82SHB2W2C2XN&pd_rd_r=4c7c434c-2ef2-4c54-be40-d280a53b0e68&pd_rd_wg=LHvkc&pd_rd_i=B078ZT22SS&psc=1",
            }
]

def get_role_by_email(email):
    return "admin"
    
def get_article_by_id(id):
    for i in get_data():
        if i["id"] == id:
            return i
def update_article(id, article_name, company, url, price, img):
    for i in get_data():
        if i['id'] == id:
            i['name'] = article_name
            i['company'] = company
            i['price'] = price
            i['url'] = url
            i['img'] = img
def get_data():
    return liste_article

def get_article_favorie_by_id(id):
    if len(liste_article_favorie) == 0:
        return None
def add_to_favorie(id, article_name, company, url, price, img):
    liste_article_favorie.append({
            "id" : id,
            "img" : img,
            "name" : article_name,
            "company" : company,
            "price" : price,
            "url" : url,
            })